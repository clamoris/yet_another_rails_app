class CreateTodoLists < ActiveRecord::Migration[5.0]
  def change
    create_table :todo_lists do |t|
      t.string :title, null: false, default: ''
      t.boolean :closed, null: false, default: false
      t.boolean :private, null: false, default: false

      t.timestamps
    end
  end
end
