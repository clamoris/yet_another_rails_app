# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user_role  = Role.create({name: 'User', description: 'Can read and create items. Can update and destroy own items'})
admin_role = Role.create({name: 'Admin', description: 'Can perform any CRUD operation on any resource'})

u1 = User.create(name: 'Sally', email: 'sally@example.com', password: 'aaaaaaaa', password_confirmation: 'aaaaaaaa', role_id: admin_role.id, authentication_token: 'sally')
u2 = User.create(name: 'Sue', email: 'sue@example.com', password: 'aaaaaaaa', password_confirmation: 'aaaaaaaa', role_id: user_role.id, authentication_token: 'sue')
u3 = User.create(name: 'Kev', email: 'kev@example.com', password: 'aaaaaaaa', password_confirmation: 'aaaaaaaa', role_id: user_role.id, authentication_token: 'kev')
u4 = User.create(name: 'Jack', email: 'jack@example.com', password: 'aaaaaaaa', password_confirmation: 'aaaaaaaa', role_id: user_role.id, authentication_token: 'jack')

TodoList.create(title: 'ToDo list 1', user_id: u1.id, private: true, closed: false, todo_items_attributes: [ {title: 'foo'} ])
TodoList.create(title: 'ToDo list 2', user_id: u1.id, private: false, closed: false, todo_items_attributes: [ {title: 'bar'} ])
TodoList.create(title: 'ToDo list 3', user_id: u2.id, private: false, closed: true, todo_items_attributes: [ {title: 'baz'} ])
TodoList.create(title: 'ToDo list 4', user_id: u3.id, private: false, closed: false, todo_items_attributes: [ {title: 'que'} ])
TodoList.create(title: 'ToDo list 5', user_id: u4.id, private: true, closed: false, todo_items_attributes: [ {title: 'nar'} ])