Rails.application.routes.draw do
  root to: 'todo_lists#index'

  devise_for :users
  scope '/admin' do
    resources :users
  end

  resources :todo_lists do
    member do
      put :close
    end
    resources :todo_items, only: [:destroy] do
      member do
        put :complete
      end
    end
  end
end
