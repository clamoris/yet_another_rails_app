class TodoItemsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  before_action :check_availability

  def complete
    @todo_item.complete!

    redirect_to @todo_list
  end

  def destroy
    @todo_item.destroy!

    redirect_to @todo_list
  end

  private

  def check_availability
    @todo_list = TodoList.available(current_user).find(params[:todo_list_id])

    @todo_item = TodoItem.find(params[:id])
    raise ActiveRecord::RecordNotFound unless @todo_item.todo_list_id == @todo_list.id
  end
end
