class TodoListsController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :set_todo_list, only: [:show, :update, :close]

  load_and_authorize_resource

  def index
    @todo_lists = TodoList.available(current_user)
    render json: @todo_lists
  end

  def show
    render json: @todo_list, include: { todo_items: { only: %i(id title completed) } }
  end

  def create
    @todo_list = TodoList.new(todo_list_params)
    @todo_list.user_id = current_user.id
    @todo_list.save!

    redirect_to @todo_list
  end

  def update
    @todo_list.update!(todo_list_params)

    redirect_to @todo_list
  end

  def destroy
    authorize! :destroy, @todo_list

    @todo_list.destroy!

    redirect_to todo_lists_path
  end

  def close
    authorize! :edit, @todo_list

    @todo_list.close!

    redirect_to @todo_list
  end

  private

  def set_todo_list
    @todo_list = TodoList.available(current_user).find(params[:id])
  end

  def todo_list_params
    params.require(:todo_list).permit(:title, :private, todo_items_attributes: [:title, :completed])
  end
end
