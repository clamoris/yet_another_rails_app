class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions

  acts_as_token_authentication_handler_for User, if: -> do
    request.headers['X-User-Token'].present? || params[:user_token].present?
  end

  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |_exception|
    render json: { success: false, error: 'Access denied' }, status: 401
  end

  rescue_from ActiveRecord::RecordNotFound do
    render json: { success: false, error: 'Not found' }, status: 404
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end
end
