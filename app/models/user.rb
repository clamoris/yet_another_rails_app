class User < ApplicationRecord
  acts_as_token_authenticatable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :role
  has_many :todo_lists

  delegate :admin?, :user?, to: 'role.name'

  validates_presence_of :name
  before_save :assign_role

  def assign_role
    self.role = Role.find_by(name: 'User') unless role
  end
end

