class TodoItem < ApplicationRecord
  belongs_to :todo_list, optional: true
  has_one :user, through: :todo_list

  TITLE_MIN_LENGTH = 3

  validates :title, presence: true, length: { minimum:  TITLE_MIN_LENGTH }

  def complete!
    update!(completed: true)
  end
end
