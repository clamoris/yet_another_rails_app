class Ability
  include CanCan::Ability

  def initialize(user)
    if user&.admin?
      can :manage, :all
    else
      can :read, TodoList, private: false
      can :manage, TodoList, private: true, user_id: user.id if user

      can :manage, TodoItem do |item|
        item.user.id == user.id
      end
    end
  end
end