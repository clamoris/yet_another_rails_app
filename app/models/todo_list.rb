class TodoList < ApplicationRecord
  has_many :todo_items, dependent: :destroy
  accepts_nested_attributes_for :todo_items

  belongs_to :user

  scope :available, ->(user) do
    user&.admin? ? all : where(private: false).or(where(user: user))
  end

  TITLE_MIN_LENGTH = 3

  validates :title, presence: true, length: { minimum:  TITLE_MIN_LENGTH }

  def close!
    update!(closed: true)
  end
end
