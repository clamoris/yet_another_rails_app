require 'rails_helper'

RSpec.describe TodoListsController, type: :controller do
  let!(:user) { create(:user) }
  let!(:admin) { create(:user, :admin) }
  let!(:public_list) { create(:todo_list, :public) }
  let!(:private_list) { create(:todo_list, :private) }
  let!(:owned_list) { create(:todo_list, :private, user: user) }

  context 'GET #index' do
    it 'show public lists' do
      get :index
      expect(json.count).to eq(1)
      expect(json.first['id']).to eq(public_list.id)
    end

    it 'show public and owned lists' do
      login_with(user)

      get :index
      expect(json.count).to eq(2)
      expect(json.first['id']).to eq(public_list.id)
      expect(json.second['id']).to eq(owned_list.id)
    end

    it 'admin could see all' do
      login_with(admin)

      get :index
      expect(json.count).to eq(3)
      expect(json.first['id']).to eq(public_list.id)
      expect(json.second['id']).to eq(private_list.id)
      expect(json.third['id']).to eq(owned_list.id)
    end
  end

  context 'GET #show' do
    it 'show public list' do
      get :show, id: public_list.id
      expect(json['id']).to eq(public_list.id)
    end

    it 'show owned list' do
      login_with(user)

      get :show, id: owned_list.id

      expect(json['id']).to eq(owned_list.id)
    end

    it 'do not show private list of other users' do
      login_with(user)

      get :show, id: private_list.id

      expect(json['id']).to be_falsey
      expect(json['error']).to eq('Not found')
      expect(json['success']).to be_falsey
    end

    it 'admin could see all' do
      login_with(admin)

      get :show, id: private_list.id

      expect(json['id']).to eq(private_list.id)
    end
  end

  context 'POST #create' do
    it 'guest could not create a list' do
      post :create, todo_list: FactoryGirl.attributes_for(:todo_list)

      expect(response.status).to eq(401)
    end

    it 'user could create a list' do
      login_with(user)

      expect { post :create, todo_list: FactoryGirl.attributes_for(:todo_list) }
          .to change { TodoList.count }.by(1)

      expect(response).to redirect_to(TodoList.last)
    end

    it 'user could create a list with items' do
      login_with(user)

      params = FactoryGirl.attributes_for(:todo_list)
      params.merge!(todo_items_attributes: [{ title: 'Write spec', completed: true }])
      post :create, todo_list: params

      item = TodoList.last.todo_items.first

      expect(item.title).to eq('Write spec')
      expect(item.completed).to be_truthy
    end
  end

  context 'DELETE #destroy' do
    it 'guest could not delete a list' do
      delete :destroy, id: public_list.id

      expect(response.status).to eq(401)
    end

    it 'user could delete owned list' do
      login_with(user)

      expect{ delete :destroy, id: owned_list }
          .to change { TodoList.count }.by(-1)

      expect(response).to redirect_to(todo_lists_path)
    end

    it 'user could not delete unowned list' do
      login_with(user)

      expect{ delete :destroy, id: public_list }
          .to change { TodoList.count }.by(0)

      expect(response.status).to eq(401)
    end

    it 'admin could delete any list' do
      login_with(admin)

      expect{ delete :destroy, id: public_list }
          .to change { TodoList.count }.by(-1)

      expect(response).to redirect_to(todo_lists_path)
    end
  end

  context 'PUT #update' do
    it 'guest could not update a list' do
      put :update, id: public_list.id, title: 'New title'

      expect(response.status).to eq(401)
    end

    it 'user could update owned list' do
      login_with(user)

      put :update, id: owned_list.id, todo_list: { title: 'New title' }

      expect(response).to redirect_to(owned_list)
      expect(owned_list.reload.title).to eq('New title')
    end

    it 'user could not update unowned list' do
      login_with(user)

      put :update, id: public_list.id, todo_list: { title: 'New title' }

      expect(response.status).to eq(401)
    end

    it 'admin could update any list' do
      login_with(admin)

      put :update, id: public_list.id, todo_list: { title: 'Admin title' }

      expect(response).to redirect_to(public_list)
      expect(public_list.reload.title).to eq('Admin title')
    end
  end

  context 'PUT #close' do
    it 'guest could not close a list' do
      put :close, id: public_list.id

      expect(response.status).to eq(401)
    end

    it 'user could close owned list' do
      login_with(user)

      put :close, id: owned_list.id

      expect(response).to redirect_to(owned_list)
      expect(owned_list.reload.closed).to be_truthy
    end

    it 'user could not close unowned list' do
      login_with(user)

      put :close, id: public_list.id

      expect(response.status).to eq(401)
    end

    it 'admin could close any list' do
      login_with(admin)

      put :close, id: public_list.id

      expect(response).to redirect_to(public_list)
      expect(public_list.reload.closed).to be_truthy
    end
  end
end
