require 'rails_helper'

RSpec.describe TodoItemsController, type: :controller do
  let!(:user) { create(:user) }
  let!(:admin) { create(:user, :admin) }

  let!(:todo_items) {
    [
      create(:todo_item, title: 'Item 1'),
       create(:todo_item, title: 'Item 2'),
       create(:todo_item, title: 'Item 3'),
    ]
  }

  let!(:owned_list) do
    create(:todo_list,
           :public,
           user: user,
           todo_items: todo_items
    )
  end

  context 'PUT #complete' do
    it 'guest could not complete an item' do
      put :complete, todo_list_id: owned_list.id, id: todo_items[0].id

      expect(response.status).to eq(401)
    end

    it 'user could complete an item in owned list' do
      login_with(user)

      put :complete, todo_list_id: owned_list.id, id: todo_items[1].id

      expect(response).to redirect_to(owned_list)
      expect(todo_items[1].reload.completed).to be_truthy
    end
  end

  context 'DELETE #destroy' do
    it 'guest could not complete an item' do
      expect{ delete :destroy, todo_list_id: owned_list.id, id: todo_items[0].id }
        .to change { TodoItem.count }.by(0)

      expect(response.status).to eq(401)
    end

    it 'user could complete an item in owned list' do
      login_with(user)

      expect{ delete :destroy, todo_list_id: owned_list.id, id: todo_items[1].id }
        .to change { TodoItem.count }.by(-1)

      expect(response).to redirect_to(owned_list)
      expect(owned_list.todo_items.count).to eq(2)
    end
  end
end
