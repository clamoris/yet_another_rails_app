FactoryGirl.define do
  sequence :name do |n|
    "person#{n}"
  end
  sequence :email do |n|
    "person#{n}@example.com"
  end

  factory :user, :class => 'User' do
    name
    email
    role
    password '12345678'
    password_confirmation '12345678'

    trait(:admin) do
      role { create(:role, name: 'Admin') }
    end
  end

  factory :role, :class => 'Role' do
    name 'user'
  end

  factory :todo_list, :class => 'TodoList' do
    title 'ToDo List'
    trait(:public) { add_attribute(:private) { false } }
    trait(:private) { add_attribute(:private) { true } }
    user
  end

  factory :todo_item, :class => 'TodoItem' do
    title 'ToDo Item'
    trait(:completed) { add_attribute(:completed) { true } }
  end
end